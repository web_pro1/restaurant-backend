import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateKitchenDto } from './dto/create-kitchen.dto';
import { UpdateKitchenDto } from './dto/update-kitchen.dto';
import { Kitchen } from './entities/kitchen.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from 'src/orders/entities/order.entity';
import { Repository } from 'typeorm/repository/Repository';
import { OrderItem } from 'src/order-items/entities/order-item.entity';

@Injectable()
export class KitchensService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,

    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,

    @InjectRepository(Kitchen)
    private kitchensRepository: Repository<Kitchen>,
  ) {}
  create(createKitchenDto: CreateKitchenDto) {
    return this.kitchensRepository.save(createKitchenDto);
  }

  findAll() {
    return this.kitchensRepository.find();
  }

  findOne(id: number) {
    return this.kitchensRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateKitchenDto: UpdateKitchenDto) {
    const customer = await this.kitchensRepository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    const updatedCustomer = { ...customer, ...updateKitchenDto };
    return this.kitchensRepository.save(updatedCustomer);
  }

  async remove(id: number) {
    const kitchen = await this.kitchensRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedOrderItem = await this.kitchensRepository.remove(kitchen);
      return deletedOrderItem;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
