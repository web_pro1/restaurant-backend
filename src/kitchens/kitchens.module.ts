import { Module } from '@nestjs/common';
import { KitchensService } from './kitchens.service';
import { KitchensController } from './kitchens.controller';
import { Kitchen } from './entities/kitchen.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from 'src/orders/entities/order.entity';
import { OrderItem } from 'src/order-items/entities/order-item.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Order, Kitchen, OrderItem])],
  controllers: [KitchensController],
  providers: [KitchensService],
})
export class KitchensModule {}
