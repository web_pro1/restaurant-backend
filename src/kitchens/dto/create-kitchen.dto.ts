import { IsNotEmpty } from 'class-validator';

export class CreateKitchenDto {
  @IsNotEmpty()
  name: string;

  amount: number;

  chef: string;
}
