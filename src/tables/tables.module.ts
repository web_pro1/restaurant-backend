import { Module } from '@nestjs/common';
import { TablesService } from './tables.service';
import { TablesController } from './tables.controller';
import { Order } from 'src/orders/entities/order.entity';
import { Table } from './entities/table.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderItem } from 'src/order-items/entities/order-item.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Order, Table, OrderItem])],
  controllers: [TablesController],
  providers: [TablesService],
})
export class TablesModule {}
