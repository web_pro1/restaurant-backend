import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTableDto } from './dto/create-table.dto';
import { UpdateTableDto } from './dto/update-table.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Table } from './entities/table.entity';

@Injectable()
export class TablesService {
  constructor(
    @InjectRepository(Table)
    private tablesRepository: Repository<Table>,
  ) {}

  create(createTableDto: CreateTableDto) {
    const newTable = new Table();
    newTable.table_name = createTableDto.table_name;
    newTable.seat = createTableDto.seat;
    newTable.status = 'available';
    return this.tablesRepository.save(newTable);
  }

  findAll(option) {
    return this.tablesRepository.find(option);
  }

  findOne(id: number) {
    return this.tablesRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateTableDto: UpdateTableDto) {
    try {
      const updatedTable = await this.tablesRepository.save({
        id,
        ...updateTableDto,
      });
      return updatedTable;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const table = await this.tablesRepository.findOneBy({ id: id });
    try {
      const deletedTable = await this.tablesRepository.remove(table);
      return deletedTable;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
