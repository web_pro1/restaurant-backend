import { Menu } from 'src/menus/entities/menu.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';
import { Order } from 'src/orders/entities/order.entity';

@Entity()
export class Table {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  table_name: string;

  @Column()
  status: string;

  @Column()
  date: string;

  @Column()
  time: string;

  @Column()
  seat: number;

  @OneToMany(() => Order, (order) => order.tables)
  orders: Order[];

  // @OneToOne(() => Order)
  // order: Order;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;
}
