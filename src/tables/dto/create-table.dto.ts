import { IsNotEmpty, Length } from 'class-validator';

export class CreateTableDto {
  table_name: string;

  seat: number;

  // @IsNotEmpty()
  status: string;

  date: string;

  time: string;
}
