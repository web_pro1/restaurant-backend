import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Order } from './entities/order.entity';
import { Menu } from 'src/menus/entities/menu.entity';
import { Table } from 'src/tables/entities/table.entity';
import { OrderItem } from 'src/order-items/entities/order-item.entity';
import { CreateOrderItemDto } from 'src/order-items/dto/create-order-item.dto';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    @InjectRepository(Table)
    private tablesRepository: Repository<Table>,
    @InjectRepository(Menu)
    private menusRepository: Repository<Menu>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    console.log(createOrderDto);
    const table = await this.tablesRepository.findOne({
      where: {
        id: createOrderDto.tableId,
      },
    });

    // console.log(createOrderDto);
    console.log(table);
    const order = new Order();
    order.amount = 0;
    order.total = 0;
    order.tableId = table.id;
    order.tables = table;
    order.status = 'wait';
    await this.ordersRepository.save(order); // ได้id
    // console.log(order);
    // console.log(createOrderDto);
    for (const od of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.amount = od.amount;
      orderItem.menu = await this.menusRepository.findOneBy({
        id: od.menuId,
      });
      orderItem.name = orderItem.menu.name;
      orderItem.price = orderItem.menu.price;
      orderItem.total = orderItem.price * orderItem.amount;
      orderItem.status = 'wait';
      orderItem.tableId = table.id;
      orderItem.order = order; // อ้างกลับ
      await this.orderItemsRepository.save(orderItem);
      order.amount = order.amount + orderItem.amount;
      order.total = order.total + orderItem.total;
    }
    await this.ordersRepository.save(order); // ได้ id
    // console.log(order);
    return await this.ordersRepository.findOne({
      where: { id: order.id },
      relations: ['tables', 'orderItems'],
    });
  }

  findAll() {
    return this.ordersRepository.find({
      relations: ['tables', 'orderItems'],
    });
  }

  findOne(id: number) {
    return this.ordersRepository.findOne({
      where: { id: id },
      relations: ['tables', 'orderItems'],
    });
  }

  findByTable(id: number) {
    return this.ordersRepository.findOne({
      where: { tableId: id },
      relations: ['tables', 'orderItems'],
    });
  }

  async update(id: number, updateOrderDto: UpdateOrderDto) {
    try {
      const updatedOrder = await this.ordersRepository.save({
        id,
        ...updateOrderDto,
      });
      return updatedOrder;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const order = await this.ordersRepository.findOneBy({ id: id });
    return this.ordersRepository.softRemove(order);
  }
}
