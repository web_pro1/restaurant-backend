import { Booking } from 'src/booking/entities/booking.entity';
import { OrderItem } from 'src/order-items/entities/order-item.entity';
import { Table } from 'src/tables/entities/table.entity';

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  amount: number;

  @Column({ type: 'float' })
  total: number;

  @Column()
  status: string;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  // @OneToOne(() => Booking)
  // booking: Booking;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItems: OrderItem[];
  // 1Order I-E OrderItem
  // OrderItem 3-I Order
  @ManyToOne(() => Table, (table) => table.orders)
  tables: Table;

  @Column()
  tableId: number;
}
