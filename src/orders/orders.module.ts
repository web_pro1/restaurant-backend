import { Module } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { Menu } from 'src/menus/entities/menu.entity';
import { Kitchen } from 'src/kitchens/entities/kitchen.entity';
import { Table } from 'src/tables/entities/table.entity';
import { OrderItem } from 'src/order-items/entities/order-item.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Order, OrderItem, Menu, Table, Kitchen])],
  controllers: [OrdersController],
  providers: [OrdersService],
})
export class OrdersModule {}
