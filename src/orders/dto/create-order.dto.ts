// class CreateOrderItemDto {
//   menuId: number;
//   amount: number;

import { CreateOrderItemDto } from 'src/order-items/dto/create-order-item.dto';

// }
export class CreateOrderDto {
  // @IsNotEmpty()
  tableId: number;
  status: string;
  orderItems: CreateOrderItemDto[];
}
