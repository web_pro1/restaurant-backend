import { IsNotEmpty, Length } from 'class-validator';

export class CreateBookingDto {
  table_name: string;

  seat: number;

  @IsNotEmpty()
  status: string;

  book_date: string;

  book_time: string;
}
