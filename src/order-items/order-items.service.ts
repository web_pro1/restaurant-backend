import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateOrderItemDto } from './dto/create-order-item.dto';
import { UpdateOrderItemDto } from './dto/update-order-item.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { OrderItem } from './entities/order-item.entity';
import { Repository, Table } from 'typeorm';
import { Menu } from 'src/menus/entities/menu.entity';
import { Order } from 'src/orders/entities/order.entity';

@Injectable()
export class OrderItemsService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    // @InjectRepository(Table)
    // private tablesRepository: Repository<Table>,
    // @InjectRepository(Menu)
    // private menusRepository: Repository<Menu>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
  ) {}

  create(createOrderItemDto: CreateOrderItemDto) {
    const newOrderItem = new OrderItem();
    newOrderItem.amount = 0;
    newOrderItem.total = 0;
    newOrderItem.status = 'wait';

    return this.orderItemsRepository.save(newOrderItem);
  }

  findAll() {
    return this.orderItemsRepository.find();
  }

  findOne(id: number) {
    return this.ordersRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateOrderItemDto: UpdateOrderItemDto) {
    try {
      const updatedOrderItem = await this.orderItemsRepository.save({
        id,
        ...updateOrderItemDto,
      });
      return updatedOrderItem;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const OrderItem = await this.orderItemsRepository.findOneBy({ id: id });
    return this.orderItemsRepository.softRemove(OrderItem);
  }
}
