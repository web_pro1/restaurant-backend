export class CreateOrderItemDto {
  menuId: number;
  amount: number;
  status: string;
  chef: string;
}
