import { Module } from '@nestjs/common';
import { OrderItemsService } from './order-items.service';
import { OrderItemsController } from './order-items.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from 'src/orders/entities/order.entity';
import { OrderItem } from './entities/order-item.entity';
import { Menu } from 'src/menus/entities/menu.entity';
import { Table } from 'src/tables/entities/table.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Order, OrderItem, Menu, Table])],
  controllers: [OrderItemsController],
  providers: [OrderItemsService],
})
export class OrderItemsModule {}
