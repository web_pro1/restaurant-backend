import { Menu } from 'src/menus/entities/menu.entity';
import { Order } from 'src/orders/entities/order.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class OrderItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column()
  status: string;

  @Column({ type: 'float' })
  price: number;

  @Column()
  amount: number;

  @Column()
  total: number;

  @Column()
  tableId: number;

  // @ManyToOne(() => Chef, (chef) => chef.orderItems)
  @Column()
  chef: string;

  @ManyToOne(() => Order, (order) => order.orderItems)
  order: Order;

  @ManyToOne(() => Menu, (menu) => menu.orderItems)
  menu: Menu; // Menu Id

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;
}
