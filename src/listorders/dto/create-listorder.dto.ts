import { IsNotEmpty, Length, IsNumber, Min } from 'class-validator';
export class CreateListorderDto {
  @IsNotEmpty()
  name: string;

  amount: number;
}
