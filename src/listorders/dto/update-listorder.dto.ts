import { PartialType } from '@nestjs/mapped-types';
import { CreateListorderDto } from './create-listorder.dto';

export class UpdateListorderDto extends PartialType(CreateListorderDto) {}
