import { Test, TestingModule } from '@nestjs/testing';
import { ListordersController } from './listorders.controller';
import { ListordersService } from './listorders.service';

describe('ListordersController', () => {
  let controller: ListordersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ListordersController],
      providers: [ListordersService],
    }).compile();

    controller = module.get<ListordersController>(ListordersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
