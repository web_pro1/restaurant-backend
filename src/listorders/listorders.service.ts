import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateListorderDto } from './dto/create-listorder.dto';
import { UpdateListorderDto } from './dto/update-listorder.dto';
import { Listorder } from './entities/listorder.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Menu } from 'src/menus/entities/menu.entity';
import { Order } from 'src/orders/entities/order.entity';
import { OrderItem } from 'src/order-items/entities/order-item.entity';

@Injectable()
export class ListordersService {
  constructor(
    @InjectRepository(Listorder)
    private listordersRepository: Repository<Listorder>,

    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,

    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,

    @InjectRepository(Menu)
    private menusRepository: Repository<Menu>,
  ) {}
  create(createListorderDto: CreateListorderDto) {
    return this.listordersRepository.save(createListorderDto);
  }

  findAll() {
    return this.listordersRepository.find();
  }

  findOne(id: number) {
    return this.listordersRepository.findOne({ where: { id: id } });
  }

  update(id: number, updateListorderDto: UpdateListorderDto) {
    return `This action updates a #${id} listorder`;
  }

  async remove(id: number) {
    const listorder = await this.listordersRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedOrderItem = await this.listordersRepository.remove(
        listorder,
      );
      return deletedOrderItem;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
