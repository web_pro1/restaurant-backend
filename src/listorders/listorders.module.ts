import { Module } from '@nestjs/common';
import { ListordersService } from './listorders.service';
import { ListordersController } from './listorders.controller';
import { Menu } from 'src/menus/entities/menu.entity';
import { Order } from 'src/orders/entities/order.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Listorder } from './entities/listorder.entity';
import { OrderItem } from 'src/order-items/entities/order-item.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Listorder, Order, OrderItem, Menu])],
  controllers: [ListordersController],
  providers: [ListordersService],
})
export class ListordersModule {}
