import { Category } from 'src/categories/entities/category.entity';
import { OrderItem } from 'src/order-items/entities/order-item.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Menu {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column({
    type: 'float',
  })
  price: number;

  @Column({
    length: '32',
  })
  type: string;

  @Column({
    // length: '128',
    default: 'No_Image_Available.jpg',
  })
  image: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.menu)
  orderItems: OrderItem[];
  //1Menu I-E OrderItem
  //OrderItem 3-I Menu

  @ManyToOne(() => Category, (category) => category.menu)
  category: Category;
  //menu 3-1 Category

  @Column()
  categoryId: number;
}
