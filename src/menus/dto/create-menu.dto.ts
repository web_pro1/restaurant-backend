import { IsNotEmpty, Length, IsNumber, Min } from 'class-validator';

export class CreateMenuDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNotEmpty()
  price: number;

  image = 'No_Image_Available.jpg';

  @IsNotEmpty()
  categoryId: number;
}
