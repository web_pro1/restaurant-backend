import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from 'src/categories/entities/category.entity';
import { Repository } from 'typeorm';
import { CreateMenuDto } from './dto/create-menu.dto';
import { UpdateMenuDto } from './dto/update-menu.dto';
import { Menu } from './entities/menu.entity';

@Injectable()
export class MenusService {
  constructor(
    @InjectRepository(Menu)
    private menusRepository: Repository<Menu>,
    @InjectRepository(Category)
    private categorysRepository: Repository<Category>,
  ) {}

  async create(createMenuDto: CreateMenuDto) {
    const category = await this.categorysRepository.findOne({
      where: {
        id: createMenuDto.categoryId,
      },
    });
    const newMenu = new Menu();
    newMenu.name = createMenuDto.name;
    newMenu.image = createMenuDto.image;
    newMenu.price = createMenuDto.price;
    newMenu.category = category;
    return this.menusRepository.save(newMenu);
  }

  findAll(option) {
    return this.menusRepository.find(option);
  }

  findByCategory(id: number) {
    return this.menusRepository.find({ where: { categoryId: id } });
  }

  findOne(id: number) {
    return this.menusRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateMenuDto: UpdateMenuDto) {
    try {
      const updatedMenu = await this.menusRepository.save({
        id,
        ...updateMenuDto,
      });
      return updatedMenu;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const menu = await this.menusRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedMenu = await this.menusRepository.remove(menu);
      return deletedMenu;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
