import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { OrdersModule } from './orders/orders.module';
import { Order } from './orders/entities/order.entity';

import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';
import { AuthModule } from './auth/auth.module';
import { ServesModule } from './serves/serves.module';
import { ListordersModule } from './listorders/listorders.module';
import { BookingModule } from './booking/booking.module';
import { Booking } from './booking/entities/booking.entity';
import { MenusModule } from './menus/menus.module';
import { Menu } from './menus/entities/menu.entity';
import { TestttttttModule } from './testtttttt/testtttttt.module';
import { CategoriesModule } from './categories/categories.module';
import { Category } from './categories/entities/category.entity';
import { TablesModule } from './tables/tables.module';
import { Table } from './tables/entities/table.entity';
import { Listorder } from './listorders/entities/listorder.entity';
import { Kitchen } from './kitchens/entities/kitchen.entity';
import { KitchensModule } from './kitchens/kitchens.module';
import { OrderItemsModule } from './order-items/order-items.module';
import { OrderItem } from './order-items/entities/order-item.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot(
      //   {
      //   type: 'sqlite',
      //   database: 'db.sqlite',
      //   synchronize: true,
      //   migrations: [],
      //   entities: [Customer, Product, Order, OrderItem, User],
      // }
      {
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'newgenesis_project',
        password: 'NewGenesis@2023',
        database: 'newgenesis_project',
        entities: [
          Order,
          OrderItem,
          User,
          Booking,
          Menu,
          Category,
          Table,
          Listorder,
          Kitchen,
        ],
        synchronize: true,
      },
    ),
    OrdersModule,
    UsersModule,
    AuthModule,
    ServesModule,
    ListordersModule,
    BookingModule,
    MenusModule,
    TestttttttModule,
    CategoriesModule,
    TablesModule,
    ListordersModule,
    KitchensModule,
    OrderItemsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
