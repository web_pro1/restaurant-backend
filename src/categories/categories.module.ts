import { Module } from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { CategoriesController } from './categories.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Category } from './entities/category.entity';
import { Menu } from 'src/menus/entities/menu.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Category, Menu])],
  controllers: [CategoriesController],
  providers: [CategoriesService],
})
export class CategoriesModule {}
