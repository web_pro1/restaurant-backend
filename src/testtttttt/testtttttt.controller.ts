import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { TestttttttService } from './testtttttt.service';
import { CreateTestttttttDto } from './dto/create-testtttttt.dto';
import { UpdateTestttttttDto } from './dto/update-testtttttt.dto';

@Controller('testtttttt')
export class TestttttttController {
  constructor(private readonly testttttttService: TestttttttService) {}

  @Post()
  create(@Body() createTestttttttDto: CreateTestttttttDto) {
    return this.testttttttService.create(createTestttttttDto);
  }

  @Get()
  findAll() {
    return this.testttttttService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.testttttttService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateTestttttttDto: UpdateTestttttttDto,
  ) {
    return this.testttttttService.update(+id, updateTestttttttDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.testttttttService.remove(+id);
  }
}
