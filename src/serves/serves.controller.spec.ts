import { Test, TestingModule } from '@nestjs/testing';
import { ServesController } from './serves.controller';
import { ServesService } from './serves.service';

describe('ServesController', () => {
  let controller: ServesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ServesController],
      providers: [ServesService],
    }).compile();

    controller = module.get<ServesController>(ServesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
