import { Module } from '@nestjs/common';
import { ServesService } from './serves.service';
import { ServesController } from './serves.controller';

@Module({
  controllers: [ServesController],
  providers: [ServesService],
})
export class ServesModule {}
